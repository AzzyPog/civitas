/* pegar os dados usando document.query*/
const inputName = document.querySelector("#name_list");
const inputPassword = document.querySelector("#password_list");
const inputEmail = document.querySelector("#email_list");
const inputDate = document.querySelector("#date_list");
const submitBtn = document.querySelector("#btn");
const errorMes = document.querySelector(".error");

/* função criarUser feita no arquivo user_post */
import criarUser from "/src/user_post.js"

/*impede da pagina recarregar quando o botão for apertado e da submit no dado*/
submitBtn.addEventListener('click', async (event) => {
    event.preventDefault();


    const nome = inputName.value;
    const senha = inputPassword.value;
    const email = inputEmail.value;
    const data = inputDate.value;

    if (nome === "" || senha === "" || email === "" || data === "") {
        errorMes.style.display = "flex";

    } else {
        errorMes.style.display = "none";
        await criarUser(nome, senha, email, data);
    }

});
