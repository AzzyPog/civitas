const {Router} = require('express');
const UserController = require('../controllers/userController');
const CommentController = require('../controllers/commentController');
const router = Router();


//user routes
router.get('/users', UserController.index);
router.get('/users/:id', UserController.show);
router.post('/users', UserController.create);
router.put('/users/:id', UserController.update);
router.delete('/users/:id', UserController.destroy);

//comment routes
router.post('/comments', CommentController.create);
router.put('/comments/:id', CommentController.update);
router.delete('/comments/:id', CommentController.destroy);
router.get('/comments', CommentController.index);
router.get('/comments/:id', CommentController.show);

//link and unlink comment and user routes
router.put('/comment/:CommentId/link/user/:UserId', CommentController.linkComment);
router.put('/comment/:id/unlink', CommentController.removeComment);



module.exports = router;
