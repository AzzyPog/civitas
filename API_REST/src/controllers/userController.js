const { response } = require('express');
const User = require('../models/User');
const Comment = require('../models/Comment');

//cria uma instancia do model
const create = async(req,res) => {
    try{
          const user = await User.create(req.body);
          return res.status(201).json({message: "O usuário foi cadastrado com sucesso!", user: user});
      }catch(err){
          res.status(500).json({error: err});
      }
};
//edita uma instancia ja criada da model
const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await User.update(req.body, {where: {id: id}});
        if(updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("O usuário não foi encontrado");
    }
};

//deleta uma instancia da model
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await User.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("O usuario foi deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("O usuario não foi encontrado.");
    }
};
//pega todas as instancias ja criadas até o momento
const index = async(req,res) => {
    try {
        const users = await User.findAll();
        return res.status(200).json({users});
    }catch(err){
        return res.status(500).json({err});
    }
};
//encontra uma instancia pelo id
const show = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id,{
            include: [{
                model: Comment
            }]
        });
        return res.status(200).json({user});
    }catch(err){
        return res.status(500).json({err});
    }
};



module.exports = {
    index,
    show,
    create,
    update,
    destroy,
};