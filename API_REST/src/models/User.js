const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");


const User = sequelize.define('User', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    birthday: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },
    cellphone: {
        type: DataTypes.INTEGER,
    }

});

User.associate = function(models) {
    User.hasMany(models.Comment);
  };



module.exports = User;