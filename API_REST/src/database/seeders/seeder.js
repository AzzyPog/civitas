require('../../config/dotenv')();
require('../../config/sequelize');

const SeedUser = require('./UserSeeder');
const SeedComment = require('./CommentSeeder');


(async () => {
  try {
    await SeedUser();
    await SeedComment();


  } catch(err) { console.log(err) }
})();
